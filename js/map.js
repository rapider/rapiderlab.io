var logLength = document.getElementById('log-length');
var logDuration = document.getElementById('log-duration');
// var logTotalFares = document.getElementById('log-total-fares');

let menuPanel = document.getElementById('menu-panel');
let closeButton = document.querySelector('#menu-button-close');

let originInput = document.getElementById('origin-input');
let destinationInput = document.getElementById('destination-input');

function convertSecondsToHours(seconds) {
    let hours = Math.floor(seconds / 3600);
    let remainingMinutes = Math.floor((seconds % 3600) / 60);
    let remainingSeconds = Math.floor(seconds % 60);

    // Pad the minutes and seconds with leading zeros if needed
    let hoursOutput = hours < 10 ? '0' + hours : hours;
    let minutesOutput = remainingMinutes < 10 ? '0' + remainingMinutes : remainingMinutes;
    let secondsOutput = remainingSeconds < 10 ? '0' + remainingSeconds : remainingSeconds;

    return `${hoursOutput}:${minutesOutput}:${secondsOutput}`;
}

function cancel(event) {
    // mapContainer.innerHTML = '';
    originInput.value = '';
    destinationInput.value = '';
}

function openMenu() {
    menuPanel.style.left = '0px';
    closeButton.classList.remove('icon-menu-open');
    closeButton.classList.add('icon-menu-close');
}

// Initialize the map and set its view to our chosen geographical coordinates and a zoom level
var map = L.map('map', {
    zoomControl: false
}).setView([51.505, -0.09], 13);

// Add an OpenStreetMap tile layer
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: 'Map data © <a href="https://openstreetmap.org">OpenStreetMap</a> contributors'
}).addTo(map);

// Add a new zoom control at the bottom right
L.control.zoom({
    position: 'bottomright'
}).addTo(map);

let polyline = null;
let markers = [];

function calculateRoute() {
    // Get the origin and destination inputs

    let origin = originInput.value;
    let destination = destinationInput.value;

    let orig = originInput.dataset.coords;
    let dest = destinationInput.dataset.coords;

    console.log('origin, destination: ', origin, destination);

    console.log('orig, dest: ', orig, dest);

    let routeType = localStorage.getItem('rapider-road-type') || 'driving-car';

    let url = `https://api.openrouteservice.org/v2/directions/${routeType}?api_key=5b3ce3597851110001cf6248e478f2a7872c4312a8afd0ea9bf7b1fa&start=${orig}&end=${dest}`;

    fetch(url)
        .then(response => response.json())
        .then(data => {

            if (!data.features) {
                alert('This location is Unreachable');
                return;
            }

            let coordinates = data.features[0].geometry.coordinates;
            let route = [];
            let latLngs = [];
            coordinates.forEach(coord => {
                route.push([coord[1], coord[0]]);
                latLngs.push(L.latLng(coord[1], coord[0]));
            });

            // Remove all existing markers from the map
            markers.forEach(marker => {
                map.removeLayer(marker);
            });

            markers = [];

            if (polyline) {
                map.removeLayer(polyline);
            }

            polyline = L.polyline(route, {color: 'blue'}).addTo(map);

            // Add a marker at the start of the route
            let startMarker = L.marker(route[0]).addTo(map);
            markers.push(startMarker);

            // Add a marker at the end of the route
            let endMarker = L.marker(route[route.length - 1]).addTo(map);
            markers.push(endMarker);

            // Adjust the map view to fit the entire route
            map.fitBounds(polyline.getBounds());

            // Get the total length of the route
            let totalLength = data.features[0].properties.summary.distance;
            console.log('Total length of the route: ', totalLength);
            totalLength = (totalLength / 1000).toFixed(2);

            logLength.textContent = totalLength + ' KM';

            let totalDuration = data.features[0].properties.summary.duration;

            logDuration.textContent = convertSecondsToHours(totalDuration);

            // let totalTollCost = data.features[0].properties.summary.toll;
            // logTotalFares.textContent = totalTollCost + '€';

        })
        .catch(error => console.error(error));
}

document.getElementById('center-button').addEventListener('click', function() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            let lat = position.coords.latitude;
            let lon = position.coords.longitude;
            map.setView([lat, lon], 13);
        });
    } else {
        alert('Geolocation is not supported by your browser.');
    }
});

$(function() {
    $(".coords-input").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: "https://us1.locationiq.com/v1/search.php",
                dataType: "json",
                data: {
                    q: request.term,
                    format: "json",
                    key: "pk.80bdae5a136539fb867c9a7b3ea2910d"
                },
                success: function(data) {
                    response($.map(data, function(item) {
                        return {
                            label: item.display_name,
                            value: item.display_name,
                            lat: item.lat,
                            lon: item.lon
                        };
                    }));
                }
            });
        },
        select: function(event, ui) {
            // Store the coordinates in the "data-coords" attribute
            let lat = parseFloat(ui.item.lat).toFixed(5);
            let lon = parseFloat(ui.item.lon).toFixed(5);
            // $(this).attr("data-coords", lat + "," + lon);
            $(this).attr("data-coords", lon + ',' + lat);

            // Enable the "Go" button if both fields have a "data-coords" attribute
            if ($('#origin-input').attr('data-coords') && $('#destination-input').attr('data-coords')) {
                $('#go-button').removeAttr('disabled');
            }
        },
        minLength: 2
    });
});

// orig, dest:  6.23544,32.98252 0.41010,43.69553
// origin, destination:  -0.68569,44.74173 9.20842,45.70060

// Create a menu on left click (or tap on mobile)
map.on('click', function(e) {

    // e.stopPropagation();

    // Remove any existing context menu
    let existingMenu = document.querySelector('.coords-context-menu');
    if (existingMenu) {
        document.body.removeChild(existingMenu);
    }


    const template = document.querySelector('#coords-context-menu');

    const clone = template.content.cloneNode(true);

    let coordsContextMenu = clone.querySelector('div.coords-context-menu');
    let coordsContextMenuCloseButton = clone.querySelector('button.delete');

    coordsContextMenuCloseButton.onclick = function() {
        document.body.removeChild(coordsContextMenu);
    };

    coordsContextMenu.style.top = e.originalEvent.clientY + 'px';
    coordsContextMenu.style.left = e.originalEvent.clientX + 'px';

    let setOrigin = clone.querySelector('#context-menu-origin');
    setOrigin.textContent = 'Set Origin';
    setOrigin.onclick = function() {
        openMenu();
        document.getElementById('origin-input').setAttribute('data-coords', e.latlng.lng + ',' + e.latlng.lat);
        $.ajax({
            url: "https://us1.locationiq.com/v1/reverse.php",
            data: {
                key: "pk.80bdae5a136539fb867c9a7b3ea2910d",
                lat: e.latlng.lat,
                lon: e.latlng.lng,
                format: "json"
            },
            dataType: "json",
            success: function(data) {
                document.getElementById('origin-input').value = data.display_name;
            }
        });
        document.getElementById('go-button').removeAttribute('disabled');
        document.body.removeChild(coordsContextMenu);
    };

    let setDestination = clone.querySelector('#context-menu-destination');
    setDestination.textContent = 'Set Destination';
    setDestination.onclick = function() {
        openMenu();

        document.getElementById('destination-input').setAttribute('data-coords', e.latlng.lng + ',' + e.latlng.lat);
        $.ajax({
            url: "https://us1.locationiq.com/v1/reverse.php",
            data: {
                key: "pk.80bdae5a136539fb867c9a7b3ea2910d",
                lat: e.latlng.lat,
                lon: e.latlng.lng,
                format: "json"
            },
            dataType: "json",
            success: function(data) {
                document.getElementById('destination-input').value = data.display_name;
            }
        });
        document.getElementById('go-button').removeAttribute('disabled');
        document.body.removeChild(coordsContextMenu);
    };

    document.body.appendChild(coordsContextMenu);
});

document.getElementById('menu-button').addEventListener('click', function() {
    if (menuPanel.style.left === '0px') {
        menuPanel.style.left = '-200px';
    } else {
        menuPanel.style.left = '0px';
    }
});

closeButton.onclick = function() {
    let menuPanelStyle = window.getComputedStyle(menuPanel);
    let menuPanelWidth = menuPanelStyle.getPropertyValue('width');
    if (menuPanel.style.left === '0px') {
        menuPanel.style.left = '-' + menuPanelWidth;
        this.classList.remove('icon-menu-close');
        this.classList.add('icon-menu-open');
    } else {
        menuPanel.style.left = '0px';
        this.classList.remove('icon-menu-open');
        this.classList.add('icon-menu-close');
    }
};

let routeTypeButtons = document.querySelectorAll('#route-type-button button');
let savedRouteType = localStorage.getItem('rapider-road-type') || 'driving-car';

window.onload = function() {

    routeTypeButtons.forEach(button => {

        button.classList.remove('is-active');

        if (button.value === savedRouteType) {
            button.classList.add('is-active');
        }

        // Add click event listener
        button.addEventListener('click', function() {
            // Remove the 'is-active' class from all buttons
            routeTypeButtons.forEach(btn => {
                btn.classList.remove('is-active');
            });

            // Add the 'is-active' class to the clicked button
            this.classList.add('is-active');

            // Save the route type to localStorage
            localStorage.setItem('rapider-road-type', this.value);
        });
    });
};
